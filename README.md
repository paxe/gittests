# Gitlab and Markdown

My gitlab and Markdown tests.

# H1

## H2

### H3

#### H4

##### H5

```python

print('Python Markdown!')

```

Quote
```
- Using backtick
"quote example"
```

[Link to gitlab.com](https://gitlab.com)

```
github and gitlab link to new tab don't work.
```

Go to [H1](#H1)

![Imagetest](img/imagetest.png "image test")

:zzz: :relaxed: :fork_and_knife: :school: :relaxed: :zzz:

![](https://gitlab.com/paxe/gittests/-/blob/master/videos/screenrecord.webm)

```
Video works.
```

1. Numbering list one
2. Numbering list two
	* Sublist one
	* Sublist two
3. Numbering list three

- [x] Finished task
- [ ] Unfinished task
	- [ ] Subtask 1
	- [x] Subtask 2
	- [ ] Subtask 3

|  Left align | Center align |  Right align |  Left align | Center align |  Right align |
| :---------- | :----------: | -----------: | :---------- | :----------: | -----------: |
| Cell 1      | Cell 2       | Cell 3       | Cell 4      | Cell 5       | Cell 6       |
| Cell 7      | Cell 8       | Cell 9       | Cell 10     | Cell 11      | Cell 12      |


| 1 | Description | done ?|
| :---------: | :---------- | --: |
| Cell 1      |  Cell 2     | ok  |
| Cell 3      |  Cell 4     | no  |
